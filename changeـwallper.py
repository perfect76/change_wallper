#!/usr/bin/python
import urllib
import subprocess
from time import sleep
from getpass import getuser
numberpicture=0
#This comment uses to fix the following Error on GNOME desktops.:GLib-GIO-Message: Using the 'memory' GSettings backend.  Your settings will not be saved or shared with other applications.
def rebug_error():
    subprocess.call("export GIO_EXTRA_MODULES=/usr/lib/x86_64-linux-gnu/gio/modules/",shell=True)
    print "rebug that"

rebug_error()

#This function uses for deleting the previous image. If you do not want to delete the photo, you can disable it, but be sure to change the name of the photo every time ,so that you  have not a mistake in performing (runing) the script again.
def remove_picture():
    #first argument set your name picture
     subprocess.call("rm -rf download.jpg",shell=True)
     print "remove picture"

remove_picture()
#This function takes a single entry and each entry represents a site. 6 sites are here for you. If you want to add a new site, add the conditional instructions below to the site. Just be careful that the site should have just one image and nothing else.
def get_picture():
    number_for_random_site=int(raw_input("please enter number 1(is random all),2(is random all),3(is random but havent many picture),4(is random all but is slow to download),5(is random all and slow to download and balck and withe),6(is sport and slow to download): "))
    global numberpicture
    #the second argumnet you can write your picture name
    if number_for_random_site==1:
        picture=urllib.urlretrieve("https://unsplash.it/1920/1080?random","Download.jpg")
        print "ok i get picture"
    elif number_for_random_site==2:
        picture=urllib.urlretrieve("https://source.unsplash.com/random/featured","Download.jpg")
        print "ok i get picture"
    elif number_for_random_site==3:
        picture=urllib.urlretrieve("https://picsum.photos/1920/1080","Download.jpg")
        print "ok i get picture"
    elif number_for_random_site==4:
        picture=urllib.urlretrieve("http://lorempixel.com/1920/1080/","Download.jpg")
        print "ok i get picture"
    elif number_for_random_site==5:
        picture=urllib.urlretrieve("http://lorempixel.com/g/1920/1080/","Download.jpg")
        print "ok i get picture"
    elif number_for_random_site==6:
        picture=urllib.urlretrieve("http://lorempixel.com/1920/1080/sports/","Download.jpg")
        print "ok i get picture"
    else:
        print "your number is not in my range"
        numberpicture-=1
    numberpicture+=1

get_picture()
#This function saves the photo taken from the site at home.
def save():
    global numberpicture
    if bool(numberpicture)==True:
        #the first argument you can copy your picture in where you like
        subprocess.call("cp -r Download.jpg ~" ,shell=True)
        print "ok i save that"
    else:
        print "no i cant save that please change your address file"

save()
nameuser=getuser()
print nameuser
#The function changes the background image.
def change_background():
    global nameuser
    global numberpicture
    if bool(numberpicture)==True:
        #the next of the file set your address your picture is there
        subprocess.call('gsettings set org.gnome.desktop.background picture-uri file:///home/%s/Download.jpg'%(nameuser),shell=True)
        print "i set wallpaper"
    else:
        print "i cant set that"

change_background()
